
import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/**
 * @author Andrii Kuch
 */

public class YouBotAgent extends Agent {

    private AID[] stations;
    private AID youbot_location;
    private AID target_station;
    private AID source_station;
    private AID order;

    protected void setup() {
        System.out.println("Hello! " + getAID().getName() + " reporting for duty.");

        // Register the book-selling service in the yellow pages
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        Object[] args = getArguments();
        if (args != null && args.length > 0) {
            youbot_location = new AID((String) args[0], true);
        }
        sd.setType("transport");
        sd.setName("Rocking");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        addBehaviour(new Negotiations());
    }

    private class Negotiations extends Behaviour {

        private int step = 0;
        private MessageTemplate task_request_template;
        private MessageTemplate accept_proposal_template;
        private MessageTemplate reply_from_station_template;
        private AID best_seller;
        private String best_seller_type;
        private int best_price;
        private ACLMessage current_task_in_process;
        private long offer_time = 5000;
        private long timer = 0;
        private boolean go_to_transportation = false;

        public Negotiations() {
            task_request_template = MessageTemplate.and(MessageTemplate.MatchConversationId("task-request"),
                    MessageTemplate.MatchPerformative(ACLMessage.CFP));

            reply_from_station_template = MessageTemplate.and(MessageTemplate.MatchConversationId("offer-request"),
                    MessageTemplate.MatchPerformative(ACLMessage.PROPOSE));

            accept_proposal_template = MessageTemplate.and(MessageTemplate.MatchConversationId("transport-request"),
                    MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL));
        }

        public void action() {
            switch (step) {
                //A task request is received
                case 0:
                    if (stations == null || stations.length == 0) {
                        myAgent.addBehaviour(new Find_stations_behavior());
                        return;
                    }
                    current_task_in_process = myAgent.receive(task_request_template);
                    if (current_task_in_process != null) {
                        System.out.println("Task request received!");
                        order = current_task_in_process.getSender();
                        ++step;
                    } else {
                        block();
                    }
                    break;

                //Split task request to location and tasks, and send tasks further to stations 
                case 1:
                    //System.out.println("Parcing Message");
                    String task_request_message = current_task_in_process.getContent();
                    //System.out.println("Got string: "+task_request_message);
                    String[] locations = task_request_message.split("-");

                    String[] tasks_to_do = locations[1].split(",");

                    source_station = new AID(locations[0], true);

                    for (String str : tasks_to_do) {
                        ACLMessage offer_request = new ACLMessage(ACLMessage.CFP);
                        offer_request.setConversationId("offer-request");
                        offer_request.setContent(str);

                        for (int s = 0; s < stations.length; s++) {
                            offer_request.addReceiver(stations[s]);
                            //System.out.println("Station ID: " + stations[s] + "will receive request to " + str);
                        }

                        //System.out.println("Offer Request: " + offer_request);
                        myAgent.send(offer_request);

                    }

                    //System.out.println("Step 1 is done");
                    ++step;

                    timer = System.currentTimeMillis();
                    break;

                //Get answers from stations
                case 2: {
                    //We give 1 second to replies from station to come in and pick the better one
                    int price;
                    //System.out.println("Starting the loop..." + (System.currentTimeMillis() - timer) + " " + OfferTime);
                    while (System.currentTimeMillis() - timer <= offer_time) {

                        ACLMessage reply_from_station = myAgent.receive(reply_from_station_template);
                        if (reply_from_station != null) {
                            //System.out.println("Reply from station is not null");

                            String[] reply_from_station_content = reply_from_station.getContent().split("-");
                            price = Integer.parseInt(reply_from_station_content[0]);
                            best_seller_type = reply_from_station_content[1];
                            if (!(youbot_location.equals(source_station))) {
                                price = price + 20;
                                //price increment if youbot is not at this station already;
                            }
                            if (best_seller == null || price < best_price) {
                                // This is the best offer at present
                                best_price = price;
                                best_seller = reply_from_station.getSender();
                            }
                        }


                    }
                    //System.out.println("***Step 2 is done***");
                    //System.out.println("***Best Price is: " + best_price + " from seller: " + best_seller + "***");
                    ++step;

                }
                break;

                //Send best price to order
                case 3:
                    //System.out.println("***Step 3***");
                    if (best_seller == null) {
                        step = 0;
                    } else {
                        ACLMessage cfp_reply = current_task_in_process.createReply();
                        cfp_reply.setPerformative(ACLMessage.PROPOSE);
                        cfp_reply.setContent(String.valueOf(best_price) + "-" + best_seller.getName() + "-" + best_seller_type);

                        myAgent.send(cfp_reply);
                        System.out.println(getAID().getName() + ": CFP reply is sent! -- Content: " + cfp_reply.getContent());
                        timer = System.currentTimeMillis();

                        ++step;
                    }
                    break;

                case 4:
                    //System.out.println("***Step 4***");
                    while (System.currentTimeMillis() - timer <= offer_time) {
                        ACLMessage accept_proposal = myAgent.receive(accept_proposal_template);
                        if (accept_proposal != null) {
                            System.out.println("Proposal accepted from " + accept_proposal.getContent());
                            order = accept_proposal.getSender();
                            target_station = new AID(accept_proposal.getContent(), true);
                            //send message to station
                            ACLMessage enqueue = accept_proposal.createReply();
                            enqueue.setPerformative(ACLMessage.INFORM);
                            myAgent.send(enqueue);
                            System.out.println("Confirmation sent");
                            addBehaviour(new Transportation());
                            go_to_transportation = true;
                            removeBehaviour(this);
                        }
                    }

                    if (go_to_transportation != true) {
                        System.out.println(getAID().getName() + ": Accept proposal message timeout. Looking for another CFP");
                        step = 0;
                    }
                    

            }
        }

        public boolean done() {
            return go_to_transportation;

        }
    }

    private class Find_stations_behavior extends OneShotBehaviour {

        private AID[] concat(AID[] a, AID[] b) {
            if (a == null) {
                return b;
            }
            if (b == null) {
                return a;
            }
            int aLen = a.length;
            int bLen = b.length;
            AID[] c = new AID[aLen + bLen];
            System.arraycopy(a, 0, c, 0, aLen);
            System.arraycopy(b, 0, c, aLen, bLen);
            return c;
        }

        public void action() {
            DFAgentDescription template = new DFAgentDescription();
            ServiceDescription sd = new ServiceDescription();
            String[] types = {"weld", "screw", "solder", "assemble"};
            stations = null;
            for (int t = 0; t < types.length; ++t) {
                System.out.println("Looking for stations of type " + types[t]);
                sd.setType(types[t]);
                template.addServices(sd);
                try {
                    DFAgentDescription[] result = DFService.search(myAgent, template);
                    if (result.length != 0) {
                        AID[] stations_this_type = new AID[result.length];
                        System.out.println(getAID().getName() + ": Found the following stations:");
                        for (int i = 0; i < result.length; ++i) {
                            stations_this_type[i] = result[i].getName();
                            System.out.println("> " + stations_this_type[i].getName());
                        }
                        stations = concat(stations, stations_this_type);
                    }
                } catch (FIPAException fe) {
                    fe.printStackTrace();
                }
            }

        }
    }

    private class Transportation extends Behaviour {

        private int step = 0;
        private MessageTemplate transportation_request;
        private boolean go_to_negotiations = false;

        public Transportation() {
            transportation_request = MessageTemplate.and(MessageTemplate.MatchConversationId("transport-request"),
                    MessageTemplate.MatchPerformative(ACLMessage.INFORM));
        }

        public void action() {

            System.out.println("Transportation is in progress...");

            switch (step) {
                case 0:
                    if (youbot_location.equals(source_station)) {
                        System.out.println(getAID().getName() + ": I'm already where the order is");
                    } else {
                        System.out.println(getAID().getName() + ": I am at " + youbot_location.getName());
                        System.out.println(getAID().getName() + ": order is at " + source_station.getName());
                        System.out.println(getAID().getName() + ": Driving to where the order is");
                        try {
                            Thread.sleep(3000);
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.println(getAID().getName() + ": Arrived to source station");
                    }
                    ACLMessage arrived = new ACLMessage(ACLMessage.INFORM);
                    arrived.setConversationId("arrived");
                    arrived.addReceiver(order);
                    myAgent.send(arrived);
                    youbot_location = source_station;
                    step++;
                    break;

                case 1:
                    System.out.println(getAID().getName() + ": Taking " + order.getName() + " to " + target_station.getName());
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    youbot_location = target_station;
                    System.out.println(getAID().getName() + ": Arrived with the order to the target station");
                    ACLMessage delivered = new ACLMessage(ACLMessage.INFORM);
                    delivered.setConversationId("delivered");
                    delivered.addReceiver(order);
                    myAgent.send(delivered);
                    go_to_negotiations = true;



            }



        }

        /*
         switch (this.step) {
         case 0: {
         youbot_location = this.source_station;
         System.out.println("Arrived to source station");
         ACLMessage arrived = new ACLMessage(ACLMessage.INFORM);
         arrived.setConversationId("arrived");
         arrived.addReceiver(order);
         myAgent.send(arrived);
         block(5000);
         youbot_location = this.target_station;
         System.out.println("Arrived with the order to the target station");
         arrived.setConversationId("delivered");
         myAgent.send(arrived);
         step = 0;
         }
         }
            
         */
        public boolean done() {
            return go_to_negotiations;

        }
    }
}
