
import java.util.Vector;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/**
 * @author Adam Gaier
 */

@SuppressWarnings("serial")
public class StationAgent extends Agent {

    private AID[] youBots;
    private AID[] orders;
    private boolean busy;
    private String stationAction; 	//what task the station performs
    private int taskTime;		//time in seconds to complete task once
    private int stationQueue;
    private Vector<AID> processingQueue;
    MessageTemplate mt;

    protected void setup() {
        System.out.println("Hello! station " + getAID().getName() + " is ready for orders.");

        busy = false;
        stationQueue = 0;
        Object[] args = getArguments();
        stationAction = (String) args[0];
        // These should be eventually be set by the warehouse at setup
        taskTime = Integer.valueOf((String) args[1]);

        // Register station service in the yellow pages
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType(stationAction);
        sd.setName(stationAction + " Station");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        // Add the behavior serving requests from youBot agents
        addBehaviour(new OfferRequestsServer());

        // Add the behavior serving purchase orders from youBot agents
        addBehaviour(new RecieveOrdersServer());

        // Add the enqueuing arriving orders
        addBehaviour(new OrderReciever());

        // Add the behavior processing orders
        //addBehaviour(new TaskPerformer());

        // Add ping behavior
        //addBehaviour(new PingReply());
    }

    // Put agent clean-up operations here
    protected void takeDown() {
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        // Close the GUI
        //myGui.dispose();
        // Printout a dismissal message
        System.out.println("Station-agent " + getAID().getName() + " terminating.");
    }

    /**
     * Inner class OfferRequestsServer. This is the behaviour used by Station
     * agents to serve incoming requests for offer from youBot agents. If the
     * requested task is able to be performed by the station the station agent
     * replies with a PROPOSE message specifying the price (time). Otherwise a
     * REFUSE message is sent back.
     */
    private class OfferRequestsServer extends CyclicBehaviour {

        public void action() {
            mt = MessageTemplate.MatchPerformative(ACLMessage.CFP);
            ACLMessage msg = myAgent.receive(mt);
            if (msg != null) {
                // CFP Message received. Process it
                String task = msg.getContent();
                System.out.println(getAID().getName() + ": Request recieved: " + task + " || Station Action: " + stationAction);
                ACLMessage reply = msg.createReply();

                if (task.equals(stationAction)) {
                    int price = taskTime + (taskTime * stationQueue);
                    reply.setPerformative(ACLMessage.PROPOSE);
                    reply.setContent(String.valueOf(price) + "-" + stationAction);
                    System.out.println(getAID().getName() + " Reply: " + reply.getContent());
                } else {
                    // The station doesn't do that task
                    reply.setPerformative(ACLMessage.REFUSE);
                    System.out.println(getAID().getName() + ": Cannot " + task);
                    //reply.setContent("not-available");
                    //reply.setContent(null);
                }

                myAgent.send(reply);
            } else {
                //System.out.println("STATION: Message was null");
                block();
            }
        }
    }  // End of inner class OfferRequestsServer

    /**
     * Inner class RecieveOrdersServer. This is the behaviour used by the
     * Station agents to offer acceptances from youbot agents. If the price has
     * not changed the station agent adds the order agent to the queue, and is
     * notified of this with an INFORM message. If another agent has entered the
     * queue in the mean time the price will have increased, so the original
     * offer is rejected and the youBot must ask again to receive the updated
     * price.
     *
     */
    private class RecieveOrdersServer extends CyclicBehaviour {

        public void action() {
            mt = MessageTemplate.MatchPerformative(ACLMessage.ACCEPT_PROPOSAL);
            ACLMessage msg = myAgent.receive(mt);
            if (msg != null) {

                // ACCEPT_PROPOSAL Message received. Process it
                ACLMessage reply = msg.createReply();
                String[] priceAndOrder = msg.getContent().split("-"); //<-- This should have the price AND the order ID

                double wantedPrice = Double.parseDouble(priceAndOrder[0]);
                double price = taskTime + (taskTime * stationQueue);

                System.out.println(getAID().getName() + ": Wanted Price: " + wantedPrice + " -- Real Price: " + price);

                if (Math.round(price) <= Math.round(wantedPrice)) //careful with equality comparisons of floating point numbers  
                {	// Price is as agreed or has gone down
                    AID orderID = new AID(priceAndOrder[1], true);
                    stationQueue++;
                    System.out.println(msg.getSender().getName() + " added to job list of " + getAID().getName());
                    reply.setPerformative(ACLMessage.INFORM);
                } else {	// Price went up since it was agreed (spot already sold to another agent)
                    reply.setPerformative(ACLMessage.FAILURE);
                    reply.setContent("price-increase");
                }

                myAgent.send(reply);
            } else {
                block();
            }
        }
    }  // End of inner class RecieveOrdersServer

    /**
     * Inner class OrderReciever. This behavior receives 'order-arrived'
     * messages from order when they arrive at the station, and enqueues them in
     * the 'processingQueue' to have the task performed on them.
     */
    private class OrderReciever extends CyclicBehaviour {

        public void action() {
            // Receive order-arrived message	
            mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM), MessageTemplate.MatchConversationId("order-arrived"));
            ACLMessage reply = myAgent.receive(mt);
            if (reply != null) {
                {
                    // Add sender to processingQueue
                    System.out.println(getAID().getName() + ": Sender " + reply.getSender().getName());
                    processingQueue.add(reply.getSender());

                    // Tell order and user it is enqueued (for debugging)
                    System.out.println(reply.getSender().getName() + " enqueued at " + getAID().getName());
                    reply.setPerformative(ACLMessage.INFORM);
                    reply.setContent("enqueuedAtStation-" + getAID().getName());
                    myAgent.send(reply);
                }
            } else {
                block();
            }
        }
    }  // End of inner class OrderReciever	

    /**
     * Inner class TaskPerformer. This is the behaviour used by the Station
     * agents to perform the task on the orders. If it is not busy it takes the
     * next order from the queue and 'performs' the task on it, sending the
     * order agent a message about the task that is performed on it so that it
     * can remove it from its task list.      *
     */
    private class TaskPerformer extends CyclicBehaviour {

        public void action() {

            if (!busy && !processingQueue.isEmpty()) //NULL POINTER EXCEPTION
            {
                busy = true;

                // Tell order it is busy
                ACLMessage processingOrder = new ACLMessage(ACLMessage.INFORM);
                processingOrder.addReceiver(processingQueue.firstElement());
                processingOrder.setContent("busy");
                myAgent.send(processingOrder);

                // wait for task to be completed
                try {
                    Thread.sleep(taskTime * 1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                // Tell order the task has been completed
                ACLMessage processedOrder = new ACLMessage(ACLMessage.INFORM);
                processedOrder.addReceiver(processingQueue.firstElement());
                processedOrder.setContent(stationAction);
                myAgent.send(processedOrder);

                // Tell order that it is no longer busy (this is probably not necessary)
                processingOrder.setContent("awake");
                myAgent.send(processingOrder);


                // remove order from the queue
                processingQueue.remove(0);
                stationQueue--;

                // free up station
                busy = false;
            }

        }
    }  // End of inner class TaskPerformer

    /**
     * Inner class PingReply
     *
     */
    private class PingReply extends CyclicBehaviour {

        public void action() {
            MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
            ACLMessage msg = myAgent.receive(mt);
            if (msg != null) {
                String[] greetingAndID = msg.getContent().split("-");
                ACLMessage msgBack = new ACLMessage(ACLMessage.INFORM);
                AID reciever = new AID(greetingAndID[1], true);
                msgBack.addReceiver(reciever);
                msgBack.setContent(greetingAndID[1] + "-" + greetingAndID[0]);
                myAgent.send(msgBack);
            } else {
                block();
            }
        }
    }
}  // End of inner class Ping Reply	
