import java.util.ArrayList;

public class TaskTree
{
	public TaskNode rootNode;
	
	public TaskTree()
	{
		StaticInit();
	}
	
	private void StaticInit()
	{
		rootNode = new TaskNode();
		rootNode.setTask("ship");
		
		TaskNode child = new TaskNode();
		child.setTask("weld");
		child.setParent(rootNode);
		
		TaskNode child2 = new TaskNode();
		child2.setTask("screw");
		child2.setParent(child);
		
		child2 = new TaskNode();
		child2.setTask("screw");
		child2.setParent(child);
		
		child2 = new TaskNode();
		child2.setTask("screw");
		child2.setParent(child);
		
		child = new TaskNode();
		child.setTask("solder");
		child.setParent(rootNode);
		
		child2 = new TaskNode();
		child2.setTask("weld");
		child2.setParent(child);
		
		TaskNode child3 = new TaskNode();
		child3.setTask("screw");
		child3.setParent(child2);
		
		child3 = new TaskNode();
		child3.setTask("solder");
		child3.setParent(child2);
		
		TaskNode child4 = new TaskNode();
		child4.setTask("weld");
		child4.setParent(child3);
		
		child4 = new TaskNode();
		child4.setTask("weld");
		child4.setParent(child3);
		
		child4 = new TaskNode();
		child4.setTask("screw");
		child4.setParent(child3);
	}
	
	public void removeChildNode(TaskNode node)
	{
		if(node.getChildren().size() > 0)
		{
			throw new IllegalArgumentException("Node could not be deleted, it still has children.");
		}
		
		if(node.getParent() != null)
		{
			node.getParent().removeChild(node);
		}
		else // This is the root node then.
		{
			rootNode = null;
		}
	}
	
	public ArrayList<TaskNode> getAvailableTasks()
	{
		if(rootNode == null)
		{
			return new ArrayList<TaskNode>();
		}
		
		return getChildTasks(rootNode);
	}
	
	ArrayList<TaskNode> getChildTasks(TaskNode node)
	{
		ArrayList<TaskNode> result = new ArrayList<TaskNode>();
		
		if(node.getChildren().size() == 0)
		{
			ArrayList<TaskNode> task = new ArrayList<TaskNode>();
			task.add(node);
			result.add(node);
		}
		else
		{
			for(TaskNode child : node.getChildren())
			{
				result.addAll(0,getChildTasks(child));
			}
		}
		
		return result;
	}
}