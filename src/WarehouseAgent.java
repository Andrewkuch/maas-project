
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.wrapper.AgentController;
import jade.wrapper.ContainerController;
import jade.wrapper.StaleProxyException;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/**
 * @author Sven Seele, Adam Gaier, Andrii Kuch
 */

@SuppressWarnings("serial")
public class WarehouseAgent extends Agent {

    boolean visualizeOutput = false;
    GUIBehavior guiBh;
    MessageTemplate guiMT;
    Socket serverSocket = null;
    PrintWriter serverOutputStream;
    BufferedReader serverInputStream;
    String incomingStationName;
    String OutgoingStationName;
    ArrayList<String> persistentAgents = new ArrayList<String>();

    protected void setup() {
        System.out.println("Hello! The warehouse (" + getAID().getName() + ") is up and running.");

        // Read arguments to see whether we need to connect to the gui.
        Object[] args = getArguments();
        if (args != null && args.length > 0 && args[0].equals("gui")) {
            System.out.println("Connecting to GUI");
            visualizeOutput = true;
            InitVisualization("127.0.0.1", 4711);
        }

        // Register station service in the yellow pages
        DFAgentDescription dfd = new DFAgentDescription();
        dfd.setName(getAID());
        ServiceDescription sd = new ServiceDescription();
        sd.setType("warehouse");
        sd.setName("WarehouseAgent");
        dfd.addServices(sd);
        try {
            DFService.register(this, dfd);
        } catch (FIPAException fe) {
            System.out.println(getAID().getName() + ": Could not register with DFAgent");
            fe.printStackTrace();
        }


        System.out.println(getAID().getName() + ": Starting agents.");
        ContainerController cc = getContainerController();
        AgentController ac;
        try {
            ac = cc.createNewAgent("IncomingStation1", "StationAgent", new String[]{"arrive", "0"});
            incomingStationName = ac.getName();
            persistentAgents.add(incomingStationName);
            ac.start();
            ac = cc.createNewAgent("OutgoingStation1", "StationAgent", new String[]{"ship", "10"});
            OutgoingStationName = ac.getName();
            persistentAgents.add(OutgoingStationName);
            ac.start();
            ac = cc.createNewAgent("WeldingStation1", "StationAgent", new String[]{"weld", "200"});
            persistentAgents.add(ac.getName());
            ac.start();
            ac = cc.createNewAgent("SolderingStation1", "StationAgent", new String[]{"solder", "300"});
            persistentAgents.add(ac.getName());
            ac.start();
            ac = cc.createNewAgent("ScrewingStation1", "StationAgent", new String[]{"screw", "400"});
            persistentAgents.add(ac.getName());
            ac.start();
            ac = cc.createNewAgent("youBot1", "YouBotAgent", new Object[]{incomingStationName});
            persistentAgents.add(ac.getName());
            ac.start();

            ac = cc.createNewAgent("youBot2", "YouBotAgent", new Object[]{OutgoingStationName});
            persistentAgents.add(ac.getName());
            ac.start();


            ac = cc.createNewAgent("Order1", "OrderAgent", new Object[]{incomingStationName});
            ac.start();


        } catch (StaleProxyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    // Put agent clean-up operations here
    protected void takeDown() {
        // Disconnect from the GUI.
        if (visualizeOutput) {
            CloseConnection();
        }
        // Deregister from the yellow pages
        try {
            DFService.deregister(this);
        } catch (FIPAException fe) {
            fe.printStackTrace();
        }
        // Printout a dismissal message
        System.out.println("Warehouse (" + getAID().getName() + ") terminating.");
    }

    protected void CloseConnection() {
        if (serverSocket == null) {
            return;
        }

        try {
            serverSocket.close();
            serverSocket = null;
            System.out.println(getAID().getName() + ": Socket closed.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void InitVisualization(String host, int portNumber) {
        try {
            // Establish the connection.
            serverSocket = new Socket(host, portNumber);
            serverOutputStream = new PrintWriter(serverSocket.getOutputStream(), true);
            serverInputStream = new BufferedReader(new InputStreamReader(serverSocket.getInputStream()));

            // Send the project type to the server.
            serverOutputStream.println("Rockin");

            // Prepare the behavior.
            guiMT = MessageTemplate.MatchConversationId("gui");
            guiBh = new GUIBehavior();
            addBehaviour(guiBh);

            System.out.println(getAID().getName() + ": Connected to GUI server.");
        } catch (Exception e) {
            System.out.println(getAID().getName() + ": Could not connect to server.");
            System.out.println(getAID().getName() + ": " + e.getMessage());
            visualizeOutput = false;
        }
    }

    private class GUIBehavior extends Behaviour {

        boolean disconnect = false;

        public void action() {
            // Look for messages from the GUI server.
            try {
                if (serverInputStream.ready()) {
                    String strIn = serverInputStream.readLine();
                    System.out.println(myAgent.getAID().getName() + ": Received: " + strIn);
                    String[] tokens = strIn.split(" ");
                    if (tokens[0].startsWith("youBot")) {
                        ACLMessage inform = new ACLMessage(ACLMessage.INFORM);
                        inform.setConversationId("arrived");
                        for (String agentName : persistentAgents) {
                            if (agentName.startsWith(tokens[0])) {
                                inform.addReceiver(new AID(agentName, true));
                                break;
                            }
                        }
                        myAgent.send(inform);
                        System.out.println(myAgent.getAID().getName() + ": Notified " + tokens[0] + " that it arrived at desination");
                    }
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            ACLMessage msg = myAgent.receive(guiMT);
            if (msg != null) {
                System.out.println(myAgent.getAID().getName() + ": Received: " + msg.getContent());
                if (msg.getContent().equals("disconnect")) {
                    System.out.println(myAgent.getAID().getName() + ": Closing the GUI server connection.");
                    disconnect = true;
                    return;
                }
                serverOutputStream.println(msg.getContent());
                System.out.println(myAgent.getAID().getName() + ": '" + msg.getContent() + "'");
            }
        }

        public boolean done() {
            return disconnect;
        }

        public int onEnd() {
            ((WarehouseAgent) myAgent).CloseConnection();
            return 0;
        }
    }
}
