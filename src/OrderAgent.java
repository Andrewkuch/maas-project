import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

/**
 * @author Sven Seele
 */
enum RTTSteps
{
	RTT_SND_CFP,
	RTT_RCV_PROPOSALS,
	RTT_SND_TRANSPORT_REQUEST,
	RTT_RCV_TRANSPORT_REPLY,
	RTT_RCV_FETCH_REPLY,
	RTT_RCV_DROP_REPLY,
	RTT_RCV_COMPLETION_REPLY,
	RTT_TASK_COMPLETE
}

@SuppressWarnings("serial")
public class OrderAgent extends Agent {
	private AID[] youBots;
	private boolean isCompleted;
	
	private AID warehouseAgent;
	
	private TaskTree taskTree;
	private String completedTask;
	private AID location;
	
	protected void setup()
	{
		// TODO: How to get the actual content of the order (i.e. the tree)?
		taskTree = new TaskTree();
		
		Object[] args = getArguments();
		if(args != null && args.length > 0)
		{
			location = new AID((String)args[0], true);
		}
		
		System.out.println(getAID().getName() + ": Hello! order "+getAID().getName()+ " has arrived at " + location.getName());
		
		// Get a reference to the warehouse.
		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("warehouse");
		template.addServices(sd);
		try
		{
			DFAgentDescription[] result = DFService.search(this, template);
			warehouseAgent = result[0].getName();
			System.out.println(getAID().getName() + ": Found the warehouse: " + warehouseAgent.getName());
		}
		catch(FIPAException fe)
		{
			fe.printStackTrace();
			return;
		}
		
		addBehaviour(new AssembleTask());
		//addBehaviour(new DummyBehavior(this));
		
		// Inform the GUI that a new order has arrived.
		ACLMessage guiMsg = new ACLMessage(ACLMessage.INFORM);
		guiMsg.addReceiver(warehouseAgent);
		guiMsg.setConversationId("gui");
		guiMsg.setContent(getName().split("@",2)[0] + " arrived");
		send(guiMsg);
	}
	
	// Put agent clean-up operations here
	protected void takeDown() {
		// Printout a dismissal message
		System.out.println(getAID().getName() + ": Order " + getAID().getName() + " was completed.");
		
		ACLMessage guiMsg = new ACLMessage(ACLMessage.INFORM);
		guiMsg.addReceiver(warehouseAgent);
		guiMsg.setConversationId("gui");
		guiMsg.setContent(getName().split("@",2)[0] + " shipped");
		send(guiMsg);
	}
	
	private class FindYouBotBehavior extends OneShotBehaviour
	{
		public void action()
		{
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("transport");
			template.addServices(sd);
			try {
				DFAgentDescription[] result = DFService.search(myAgent, template);
				// TODO: these should be removed later.
				System.out.println(getAID().getName() + ": Found the following youBot agents:");
				youBots = new AID[result.length];
				for (int i = 0; i < result.length; ++i) {
					youBots[i] = result[i].getName();
					System.out.println("> " + youBots[i].getName());
				}
			}
			catch (FIPAException fe) {
				fe.printStackTrace();
			}
		}
	}
	
	/**
	 * Inner class AssembleTask.
	 * @author Sven
	 * This is the behavior used by order agents to manage the assembly of the parts
	 * as well as parsing of the tree.
	 */
	private class AssembleTask extends Behaviour
	{
		Boolean isActive = true;
		ArrayList<TaskNode> currentTaskList = new ArrayList<TaskNode>();
		
		public void action()
		{
			if(!isActive)
				return;
			
		
			if(currentTaskList.size() > 0)
			{
				addBehaviour(new RequestTransportTask(getTaskList(currentTaskList), this));
				isActive = false;
			}
			else
			{
				// Parse the tree to choose a list of tasks to be performed right now and start the transport task behavior.
				currentTaskList = taskTree.getAvailableTasks();
				
				// If the task list remains empty after parsing the tree, the tree is empty and the order is assembled. 
				if(currentTaskList.size() == 0)
				{
					isCompleted = true;
				}
			}
		}
		
		private ArrayList<String> getTaskList(ArrayList<TaskNode> currentTasks)
		{
			ArrayList<String> taskList = new ArrayList<String>();
			
			for(TaskNode node : currentTasks)
			{
				taskList.add(node.getTask());
			}
			
			return taskList;
		}
		
		private void removeTaskFromTree(String completedTask)
		{
			TaskNode doneTaskNode = null;
			
			for(TaskNode node : currentTaskList)
			{
				// For not we do not care which exact task was completed, only the type matters.
				if(node.getTask().equals(completedTask))
				{
					doneTaskNode = node;
				}
			}
			
			// Remove the node from the tree and refresh the task list.
			taskTree.removeChildNode(doneTaskNode);
			currentTaskList = taskTree.getAvailableTasks();
		}
		
		public void hasFinishedCurrentTask(String completedTask)
		{
			// Check which task from the list was completed and remove it.
			// This might also require error handling.
			removeTaskFromTree(completedTask);
			isActive = true;
		}

		@Override
		public boolean done() {
			return isCompleted;
		}
	}
	
	/**
	 * Behavior for testing purposes.
	 * @author Sven
	 *
	 */
	private class DummyBehavior extends Behaviour
	{
		int step = 0;
		AID stations[];
		MessageTemplate mt;
		
		public DummyBehavior(Agent a)
		{
			DFAgentDescription template = new DFAgentDescription();
			ServiceDescription sd = new ServiceDescription();
			sd.setType("screw");
			template.addServices(sd);
			try {
				DFAgentDescription[] result = DFService.search(a, template);
				System.out.println(getAID().getName() + ": Found the following station agents:");
				stations = new AID[result.length];
				for (int i = 0; i < result.length; ++i) {
					stations[i] = result[i].getName();
					System.out.println(stations[i].getName());
				}
			}
			catch (FIPAException fe) {
				fe.printStackTrace();
				return;
			}
		}
		
		public void action()
		{
			switch(step)
			{
			case 0:
				// Send the cfp to all youBots.
				ACLMessage inform = new ACLMessage(ACLMessage.INFORM);
				inform.setConversationId("test");
				inform.setContent("hello-" + myAgent.getAID().getName());
				for (int j = 0; j < stations.length; ++j) {
					inform.addReceiver(stations[j]);
				}
				myAgent.send(inform);
				mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
				System.out.println(getAID().getName() + ": Send inform message to stations");
				step = 1;
				break;
			case 1:
				ACLMessage reply = myAgent.receive(mt);
				if(reply != null)
				{
					System.out.print(getAID().getName() + ": Received message from ");
					System.out.print(reply.getSender().getName());
					System.out.println(":");
					System.out.println("'"+ reply.getContent() + "'");
					step = 2;
				}
				else
				{
					block();
				}
			}
		}
		
		public boolean done()
		{
			return (step == 2);
		}
	}
	
	
	/**
	   Inner class RequestTask.
	   This is the behavior used by order agents to request tasks 
	   from youBot agents.
	 */
	private class RequestTransportTask extends Behaviour {		
		private AID bestSeller; // The agent who provides the best offer 
		private int bestPrice;  // The best offered price
		private AID assignedStation;
		private int repliesCnt = 0; // The counter of replies from youBot agents
		private long resendTimeout = 30000;
		private long replyTimeout = 10000;
		private long timer = 0;
		private MessageTemplate mt; // The template to receive replies
		private RTTSteps step = RTTSteps.RTT_SND_CFP;
		private ArrayList<String> taskList; // The task to be performed
		private String assignedTask; // The chosen task
		
		private ACLMessage request;
		
		private AssembleTask assembleBehavior;
		
		public RequestTransportTask(ArrayList<String> taskType, AssembleTask assembleBehavior)
		{
			this.taskList = taskType;
			this.assembleBehavior = assembleBehavior;
		}

		public void action() {
			switch (step) {
			case RTT_SND_CFP:
				// If there are no youBots, there's nothing to do.
				// TODO: This means we will need to check for new youBots somewhere, otherwise this behavior will never finish.
				if(youBots == null || youBots.length == 0)
				{
					myAgent.addBehaviour(new FindYouBotBehavior());
					return;
				}
				// Send the cfp to all youBots.
				ACLMessage cfp = new ACLMessage(ACLMessage.CFP);
				String taskStr = "";
				for(String task: new HashSet<String>(taskList))
				{
					taskStr += task + ",";
				}
				taskStr = taskStr.substring(0, taskStr.length() - 1);
				System.out.println("Task String = " + taskStr);
				cfp.setConversationId("task-request");
				cfp.setReplyWith("cfp"+System.currentTimeMillis()); // Unique value
				cfp.setContent(location.getName() + "-" + taskStr);
				
				for(int y = 0; y < youBots.length; ++y)
				{
					cfp.addReceiver(youBots[y]);
				}
				myAgent.send(cfp);
				
				System.out.println(getAID().getName() + ": Sent " + youBots.length + " CFP(s)");
				// Prepare the template to get proposals.
				mt = MessageTemplate.and(
						MessageTemplate.MatchConversationId("task-request"),
						MessageTemplate.and(
								MessageTemplate.MatchInReplyTo(cfp.getReplyWith()),
								MessageTemplate.MatchPerformative(ACLMessage.PROPOSE)));
				timer = System.currentTimeMillis();
				repliesCnt = 0;
				step = RTTSteps.RTT_RCV_PROPOSALS;
				break;
			case RTT_RCV_PROPOSALS:
				// If order waited too much for youBot replies, assume they are busy => move to next step.
				if(System.currentTimeMillis() - timer >= replyTimeout)
				{
					step = RTTSteps.RTT_SND_TRANSPORT_REQUEST;
					break;
				}
				
				// Receive all proposals/refusals from youBot agents.
				ACLMessage reply = myAgent.receive(mt);
				//System.out.println("+++ORDER: Recieved: " + mt.toString());
				if (reply != null) {
					// Reply received
					// This is an offer.
					
					String receivedMsg = reply.getContent();
					System.out.println("ORDER: Recieved: " + reply.getContent());
					String[] msgTokens = receivedMsg.split("-");
					System.out.println(getAID().getName() + ": I received:");
					for(String token : msgTokens)
					{
						System.out.println(token);
					}
					int price = Integer.parseInt(msgTokens[0]);
					if (bestSeller == null || price < bestPrice) {
						// This is the best offer at present
						bestPrice = price;
						bestSeller = reply.getSender();
						assignedStation = new AID(msgTokens[1], true);
						assignedTask = msgTokens[2];
					}
					
					++repliesCnt;
					// Compare no. of replies to sent cfps.
					if (repliesCnt >= youBots.length) {
						// We received all replies.
						step = RTTSteps.RTT_SND_TRANSPORT_REQUEST;
					}
				}

				break;
			case RTT_SND_TRANSPORT_REQUEST:
				// If there are no youBots selling their service, go back to sending cfps.
				if(bestSeller == null)
				{
					// If no youBots have responded, wait before re-sending cfps.
					block(resendTimeout);
					step = RTTSteps.RTT_SND_CFP;
				}
				else
				{
					// Send the transport request to the youBot that provided the best offer.
					request = new ACLMessage(ACLMessage.ACCEPT_PROPOSAL);
					request.addReceiver(bestSeller);
					request.setContent(assignedStation.getName());
					request.setConversationId("transport-request");
					request.setReplyWith("order"+System.currentTimeMillis());
					myAgent.send(request);
					// Prepare the template to get the transport request reply.
					mt = MessageTemplate.and(
							MessageTemplate.MatchConversationId("transport-request"),
							MessageTemplate.and(
									MessageTemplate.MatchInReplyTo(request.getReplyWith()),
									MessageTemplate.MatchPerformative(ACLMessage.INFORM)));
					step = RTTSteps.RTT_RCV_TRANSPORT_REPLY;
				}
				break;
			case RTT_RCV_TRANSPORT_REPLY:      
				// Receive the transport request reply.
				reply = myAgent.receive(mt);
				if (reply != null) {
					// Transport reply received.
					// Purchase successful, waiting for to be picked up by the youBot.
					System.out.println(getAID().getName() + ": " + assignedTask +" successfully purchased from agent "+reply.getSender().getName());
					System.out.println(getAID().getName() + ": Price = "+bestPrice);
					System.out.println(getAID().getName() + ": Waiting for " + reply.getSender().getName() + " to get me.");
					// Prepare the template to get the fetch reply.
					mt = MessageTemplate.and(
							MessageTemplate.MatchConversationId("arrived"),
							MessageTemplate.and(
									MessageTemplate.MatchSender(reply.getSender()),
									MessageTemplate.MatchPerformative(ACLMessage.INFORM)));
					step = RTTSteps.RTT_RCV_FETCH_REPLY;
				}
				else {
					block();
				}
				break;
			case RTT_RCV_FETCH_REPLY:
				// Receive the fetch reply.
				reply = myAgent.receive(mt);
				if(reply != null) {
					// Fetch reply received.
					location = reply.getSender();
					System.out.println(getAID().getName() + ": Picked up by " + reply.getSender().getName());
					System.out.println(getAID().getName() + ": Waiting to be at assinged station"); // TODO: assign assignedStation variable
					
					// Inform the warehouse where the order is now.
					ACLMessage guiMsg = new ACLMessage(ACLMessage.INFORM);
					guiMsg.addReceiver(warehouseAgent);
					guiMsg.setConversationId("gui");
					guiMsg.setContent(getName().split("@",2)[0] + " is on " + reply.getSender().getName().split("@",2)[0]);
					myAgent.send(guiMsg);
					
					// Prepare the template to get the drop off reply.
					mt = MessageTemplate.and(MessageTemplate.MatchConversationId("delivered"),
							MessageTemplate.and(
									MessageTemplate.MatchSender(reply.getSender()),
									MessageTemplate.MatchPerformative(ACLMessage.INFORM)));
					step = RTTSteps.RTT_RCV_DROP_REPLY;
				}
				else
				{
					block();
				}
				break;
			case RTT_RCV_DROP_REPLY:
				// Receive the drop off reply.
				reply = myAgent.receive(mt);
				if(reply != null)
				{
					// Drop off reply received.
					location = assignedStation;
					System.out.println(getAID().getName() + ": Order now at station " +  assignedStation);
					System.out.println(getAID().getName() + ": Waiting for current task to be completed by assigned station");
					
					// Inform the warehouse where the order is now.
					ACLMessage guiMsg = new ACLMessage(ACLMessage.INFORM);
					guiMsg.addReceiver(warehouseAgent);
					guiMsg.setConversationId("gui");
					guiMsg.setContent(getName().split("@",2)[0] + " is at " + assignedStation.getName().split("@",2)[0]);
					myAgent.send(guiMsg);
					
					// Tell the station that the order has arrived.
					ACLMessage inform = new ACLMessage(ACLMessage.INFORM);
					inform.addReceiver(assignedStation);
					inform.setConversationId("order-arrived");
					myAgent.send(inform);
					mt = MessageTemplate.and(MessageTemplate.MatchConversationId("task-complete"),
							MessageTemplate.and(
							MessageTemplate.MatchSender(assignedStation),
							MessageTemplate.MatchPerformative(ACLMessage.INFORM)));
					step = RTTSteps.RTT_RCV_COMPLETION_REPLY;
				}
				else
				{
					block();
				}
				break;
			case RTT_RCV_COMPLETION_REPLY:
				// Receive the task completion reply.
				reply = myAgent.receive(mt);
				if(reply != null)
				{
					// Task completion reply received.
					completedTask = assignedTask;
					System.out.println(getAID().getName() + ": Current task " + assignedTask + " was completed");
					step = RTTSteps.RTT_TASK_COMPLETE;
				}
				else
				{
					block();
				}
			case RTT_TASK_COMPLETE:
			default:
				// TODO: error handling?
				break;
			}
		}

		public boolean done() {
			return (step == RTTSteps.RTT_TASK_COMPLETE);
		}
		
		/**
		 * Called when the behavior has finished.
		 * It will notify the assemble behavior that the task is completed.
		 */
		public int onEnd()
		{
			assembleBehavior.hasFinishedCurrentTask(assignedTask);
			return 0;
		}
	}  // End of inner class RequestPerformer
}
