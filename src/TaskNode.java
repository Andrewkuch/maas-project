import java.util.ArrayList;

public class TaskNode
{
	private TaskNode parent = null;
	private ArrayList<TaskNode> children = new ArrayList<TaskNode>();
	private String task = "";
	
	public void setParent(TaskNode parent)
	{
		if(this.parent != null)
		{
			this.parent.removeChild(this);
		}
		
		this.parent = parent;
		this.parent.addChild(this);
	}
	
	public TaskNode getParent()
	{
		return this.parent;
	}
	
	public void addChild(TaskNode child)
	{
		if(children.contains(child))
		{
			throw new IllegalArgumentException("Task node was already in children.");
		}
		
		children.add(child);
	}
	
	public void removeChild(TaskNode child)
	{
		if(!children.contains(child))
		{
			throw new IllegalArgumentException("The task node was not in the list of children.");
		}
		
		children.remove(child);
	}
	
	public ArrayList<TaskNode> getChildren()
	{
		return children;
	}
	
	public void setTask(String task)
	{
		this.task = task;
	}
	
	public String getTask()
	{
		return this.task;
	}
	
	public ArrayList<TaskNode> getCopyOfChildren()
	{
		ArrayList<TaskNode> children = new ArrayList<TaskNode>();
		for(TaskNode child : this.children)
		{
			children.add(child);
		}
		
		return children;
	}
}